'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     static associate({ Game }) {
      // define association here
      this.hasMany(Game, { foreignKey: 'user_id' });
    }
  };
  User.init({
    name: {
      type:DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'User must have a name'},
        notEmpty: {msg: 'Name cannot be empty'}
      }
    },
    email: {
      type:DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        notNull: {msg: 'User must have an email'},
        notEmpty: {msg: 'Email cannot be empty'}
      }
    },
    password: {
      type:DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'User must have a password'},
        notEmpty: {msg: 'Password cannot be empty'}
      }
    }
  }, {
    sequelize,
    tableName: 'users',
    modelName: 'User',
  });
  return User;
};