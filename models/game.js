'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ User }) {
      // define association here
      this.belongsTo(User, {foreignKey: 'user_id'});
    }
  };
  Game.init({
    level: {
      type:DataTypes.INTEGER,
      defaultValue: 0
    },
    score: {
      type:DataTypes.INTEGER,
      defaultValue: 0
    },
    highest_score: {
      type:DataTypes.INTEGER,
      defaultValue: 0
    },
    moves: {
      type:DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    sequelize,
    modelName: 'Game',
  });
  return Game;
};