const express = require('express');
const dotenv = require('dotenv');
const userRouter = require('./routes/users');
const gameRouter = require('./routes/games');
const { sequelize } = require('./models');

const app = express();
dotenv.config();

const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.listen(PORT, async () => {
    try {
        console.log(`Server started on ${PORT}`);
        await sequelize.sync({ force: false });
        app.use('/user', userRouter);
        app.use('/game',gameRouter);
    } catch(err) {
        console.error(err);
        resizeBy.status(500).json(err);
    }
});
