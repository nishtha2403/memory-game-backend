const express = require('express');
const authenticateJWT = require('../auth');
const { User, Game}  = require('../models');

const router = express.Router();

router.get('/', authenticateJWT, async (req,res) => {
    try {
        const allGames= await Game.findAll({ attributes: ['id', 'user_id', 'level', 'score', 'highest_score', 'moves']});
        return res.json(allGames);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.get('/current', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const GameDetails = await Game.findAll({ attributes: ['id', 'user_id', 'level', 'score', 'highest_score','moves'], where: {user_id}});
        return res.json(GameDetails);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.post('/new', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const { level,score,highest_score, moves } = req.body;
        const  newGameStatus  = await Game.create({user_id,level,score,highest_score, moves});

        res.json({msg: "New Game Added"});
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.put('/:gameId', authenticateJWT, async (req,res) => {
    try {
        const id = req.params.gameId;
        const { email } = req.user;
        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const { score,highest_score, moves } = req.body;

        let gameUpdateStatus;

        [ gameUpdateStatus ] = await Game.update({ score,highest_score, moves}, { where: {id} });

        if(gameUpdateStatus){
            return res.json({msg: "Game Updated!"});
        } else {
            throw new Error('Unauthorized');
        }
    } catch(err) {
        console.error(err);
        if(err.message === 'Unauthorized') {
            return res.status(401).json({msg:'Unauthorized'});
        }
        return res.status(400).json(err);
    }
});

module.exports = router;




